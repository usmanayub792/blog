<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/welcome', 'HomeController@index');

Route::get('/profile', 'PagesController@profile');
Route::get('/create', 'PostsController@index');
Route::post('/create', 'PostsController@create');

Route::get('/posts/{id}', 'PostsController@show');

Route::get('/pages/edit/{id}', 'PostsController@edit');
Route::post('/pages/edit/{id}', 'PostsController@update');

Route::get('/pages/delete/{id}', 'PostsController@delete');