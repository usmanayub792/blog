<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Post;

class PagesController extends Controller
{
    function profile(){
    	if (Auth::check()) {
    		$posts = Post::all()->where('user_id', auth()->user()->id);
    		return view('pages.profile')->with(['name' => (auth()->user()->name), 'posts'=>$posts]);
    	}
    	else{
    		return redirect('login');
    	}
    	
    }
}
