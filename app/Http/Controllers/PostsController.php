<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Post;

class PostsController extends Controller
{
    function index(){
    	if (Auth::check()) {
    		return view('pages.create');
    	}
    	else{
    		return redirect('login');
    	}
    }

    function create(Request $data){

    	$post = new Post();
        $post->title = $data->input('title');
        $post->content = $data->input('content');
        $post->user_id = Auth()->User()->id;
        $post->save();

    	return redirect('welcome');
    }

    function show($id){
        $post = Post::find($id);
        return view('pages.open')->with('post', $post);
    }

    function edit($id){
        $post = Post::find($id);
        if ($post->user_id == Auth()->User()->id) {
            return view('pages.edit')->with('post', $post);    
        }
        else{
            return redirect('welcome');
        }
        
    }

    function update(Request $data){
        $post = Post::find($data->id);
        $post->title = $data->input('title');
        $post->content = $data->input('content');
        $post->save();
        return redirect('welcome');
    }

    function delete($id){
        $post = Post::find($id);
        if ($post->user_id == Auth()->User()->id) {
            $post->delete();
        }
        return redirect('welcome');
        
    }
}
