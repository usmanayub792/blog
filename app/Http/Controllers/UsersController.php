<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Post;

class UsersController extends Controller
{
    function index(){
    	if (Auth::check()) {
    		$posts = Post::all()->where('user_id', auth()->user()->id);
    		return view('profile')->with('posts', $posts);
    	}
    	else{
    		return redirect('login');
    	}	
    }
}
