@extends('layouts.app')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        @section('content')
        <div class="flex-center position-ref full-height">
            <div class="container">
                <div class="title m-b-md">
                    @foreach($posts as $post)
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ URL('./images/cover_image.png') }}">
                            </div>
                            <div class="col-md-10">
                                <h3> <a href="{{url('/posts/'.$post->id) }}">{{    $post->title }}</a> </h3>
                                <p> {{ $post->content }} </p>
                            </div>                            
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endsection
    </body>
</html>
