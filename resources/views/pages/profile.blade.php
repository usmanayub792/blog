

@extends('layouts.app')

@section('content')

	<div class="container">
		<h1>This is Profile of {{ $name }}</h1>
		@foreach($posts as $post)
			<div>
				<h3> {{ $post->title }} </h3>
				<p> {{ $post->content }} </p>
			</div>
        @endforeach
	</div>

@endsection