@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="form-group">
			<form method="post" action="/pages/edit/{{$post->id}}">
				<h1>Update Post</h1>
				<input type="text" name="title" class="form-control" placeholder="Title" value="{{ $post->title }}">	
				<textarea class="form-control" placeholder="Post Content" rows="10" name="content">{{ $post->content }}</textarea>
				<input type="hidden" name="id" value="{{$post->id}}">
				{{ csrf_field() }}
				<button class="btn btn-primary" type="submit">Update</button>
			</form>
		</div>
	</div>

@endsection