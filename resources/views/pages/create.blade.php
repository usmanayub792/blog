@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="form-group">
			<form method="post" action="/create">
				<h1>Create New Post</h1>
				<input type="text" name="title" class="form-control" placeholder="Title">	
				<textarea class="form-control" placeholder="Post Content" rows="10" name="content"></textarea>
				{{ csrf_field() }}
				<button class="btn btn-primary">Post</button>
			</form>
		</div>
	</div>

@endsection