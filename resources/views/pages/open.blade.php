@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<img src="{{ URL('./images/cover_image.png') }}">
			</div>
			<div class="col-md-10">
				<h3> <a href="/posts/{{ $post->id }}">{{ $post->title }}</a> </h3>
				<p> {{ $post->content }} </p>
			</div>
		</div>
		@auth
			@if($post->user_id == Auth()->User()->id)
				<a class="btn btn-primary" href="{{url('posts/'.$post->id.'/edit')}}">Edit</a>
				<a class="btn btn-danger" href="/pages/delete/{{$post->id}}">Delete</a>
			@endif
		@endauth
	</div>

@endsection